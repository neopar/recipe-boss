# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the recipe-boss package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: recipe-boss 0.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-25 08:58+0100\n"
"PO-Revision-Date: 2018-11-25 08:59+0100\n"
"Last-Translator: Paul Reiter <paul.reiter@gmx.de>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/views/View.vue:113
msgid "Are you sure your want to delete this recipe?"
msgstr "Möchten Sie dieses Rezept wirklich löschen?"

#: src/views/Edit.vue:7 src/views/Edit.vue:17 src/views/View.vue:11
msgid "Back"
msgstr "Zurück"

#: src/views/Edit.vue:181 src/views/Edit.vue:191 src/views/View.vue:117
msgid "Cancel"
msgstr "Abbrechen"

#: src/App.vue:41
msgid "Created by Brian Douglass"
msgstr "Erstellt von Brian Douglass"

#: src/views/View.vue:64 src/views/View.vue:124
msgid "Delete"
msgstr "Löschen"

#: src/views/Edit.vue:54
msgid "Description"
msgstr "Beschreibung"

#: src/views/Edit.vue:82 src/views/View.vue:90
msgid "Directions"
msgstr "Zubereitung"

#: src/views/View.vue:58
msgid "Edit"
msgstr "Bearbeiten"

#: src/views/Edit.vue:218
msgid "Edit Recipe"
msgstr "Rezept bearbeiten"

#: src/views/Edit.vue:198 src/views/View.vue:133
msgid "Error"
msgstr "Fehler"

#: src/App.vue:46
msgid "Fork this on GitLab"
msgstr ""

#: src/views/Edit.vue:128
msgid "Image Link"
msgstr "Bildverweis"

#: src/views/Edit.vue:65 src/views/View.vue:74
msgid "Ingredients"
msgstr "Zutaten"

#: src/App.vue:24
msgid "Language"
msgstr "Sprache"

#: src/views/Edit.vue:10 src/views/Edit.vue:218 src/views/List.vue:5
#: src/views/View.vue:4
msgid "New Recipe"
msgstr "Neues Rezept"

#: src/views/Edit.vue:93 src/views/View.vue:104
msgid "Notes"
msgstr "Notizen"

#: src/views/Edit.vue:201 src/views/View.vue:136
msgid "Ok"
msgstr "Ok"

#: src/views/Edit.vue:104 src/views/View.vue:33
msgid "Prep Time"
msgstr "Arbeitszeit"

#: src/views/Edit.vue:139
msgid "Rating"
msgstr "Bewertung"

#: src/views/Edit.vue:252 src/views/View.vue:180
msgid "Recipe not found"
msgstr "Rezept nicht gefunden"

#: src/views/Edit.vue:171
msgid "Save"
msgstr "Speichern"

#: src/views/List.vue:10
msgid "Search"
msgstr "Suchen"

#: src/views/Edit.vue:43 src/views/View.vue:45 src/views/View.vue:53
msgid "Source"
msgstr "Quelle"

#: src/views/Edit.vue:32
msgid "Title"
msgstr "Titel"

#: src/views/Edit.vue:116 src/views/View.vue:36
msgid "Total Time"
msgstr "Gesamtzeit"

#: src/views/Edit.vue:74
msgid ""
"Use a # to group ingredients, for example\n"
"                        \"# Dressing\", \"# Salad\""
msgstr ""
"Verwenden Sie #, um Zutaten zu gruppieren z.B.\n"
"                        \"# Dressing\", \"# Salat\""
