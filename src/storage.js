import uuid from 'uuid/v4';

import remoteStorage from './remotestorage';
import EventBus from './bus';

let metadata = {};
function loadMetadata() {
    return remoteStorage.recipes.list().then((recipes) => {
        Object.values(recipes).forEach((recipe) => {
            if (recipe.id) {
                metadata[recipe.id] = {
                    id: recipe.id,
                    image: recipe.image,
                    title: recipe.title,
                    description: recipe.description,
                    rating: recipe.rating,
                };
            }
        });

        window.localStorage.setItem('metadata', JSON.stringify(metadata));
        return metadata;
    });
}

let localStorageMetadata = window.localStorage.getItem('metadata');
if (localStorageMetadata) {
    try {
        metadata = JSON.parse(localStorageMetadata);
    }
    catch (e) {
        metadata = {};
    }
}

remoteStorage.on('disconnected', () => {
    metadata = {};
    window.localStorage.removeItem('metadata');

    EventBus.$emit('reload-recipes');
});

remoteStorage.on('connected', () => {
    loadMetadata().then(() => {
        EventBus.$emit('reload-recipes');
    });
});

remoteStorage.recipes.on('change', () => {
    loadMetadata().then(() => {
        EventBus.$emit('reload-recipes');
    });
});

remoteStorage.recipes.init();

// Remote storage doesn't seem to like the groceries module (or maybe having multiple modules?)
// TODO Using local storage directly for now

let groceries = [];
let localGroceries = window.localStorage.getItem('groceries');
if (localGroceries) {
    try {
        groceries = JSON.parse(localGroceries);
    }
    catch (e) {
        groceries = [];
    }
}

function saveGroceries() {
    window.localStorage.setItem('groceries', JSON.stringify(groceries));
}

let groceryCategories = [];
let localGroceryCategories = window.localStorage.getItem('groceryCategories');
if (localGroceryCategories) {
    try {
        groceryCategories = JSON.parse(localGroceryCategories);
    }
    catch (e) {
        groceryCategories = [];
    }
}

function saveGroceryCategories() {
    window.localStorage.setItem('groceryCategories', JSON.stringify(groceryCategories));
}

export default {
    remoteStorage: remoteStorage,

    recipes: {
        save(recipe) {
            if (!recipe.id) {
                let id = uuid();
                recipe.id = id;
            }

            if (recipe.total_time !== null && recipe.total_time !== undefined) {
                recipe.total_time = parseInt(recipe.total_time, 10);
                if (Number.isNaN(recipe.total_time)) {
                    recipe.total_time = 0;
                }
            }

            if (recipe.prep_time !== null && recipe.prep_time !== undefined) {
                recipe.prep_time = parseInt(recipe.prep_time, 10);
                if (Number.isNaN(recipe.prep_time)) {
                    recipe.prep_time = 0;
                }
            }

            return remoteStorage.recipes.add(recipe).then(() => {
                metadata[recipe.id] = {
                    id: recipe.id,
                    image: recipe.image,
                    title: recipe.title,
                    description: recipe.description,
                    rating: recipe.rating,
                };
                window.localStorage.setItem('metadata', JSON.stringify(metadata));

                return recipe;
            });
        },
        remove(id) {
            return remoteStorage.recipes.delete(id).then(() => {
                delete metadata[id];
                window.localStorage.setItem('metadata', JSON.stringify(metadata));
            });
        },
        find(id) {
            return remoteStorage.recipes.find(id);
        },
        search(term) {
            return new Promise((resolve) => {
                let results = [];
                let metadataValues = Object.values(metadata);
                for (let i = 0; i < metadataValues.length; i++) {
                    let result = metadataValues[i];

                    if (term) {
                        let title = result.title ? result.title.toLowerCase() : '';
                        let description = result.description ? result.description.toLowerCase() : '';

                        if (
                            title.indexOf(term.toLowerCase()) >= 0 ||
                            description.indexOf(term.toLowerCase()) >= 0
                        ) {
                            results.push(result);
                        }
                    }
                    else {
                        results.push(result);
                    }
                }

                results = results.sort((a, b) => {
                    if (a.title > b.title) {
                        return 1;
                    }
                    if (a.title < b.title) {
                        return -1;
                    }

                    return 0;
                });

                resolve(results);
            });
        },
    },

    groceries: {
        save(grocery) {
            if (grocery.id) {
                let index = groceries.findIndex((g) => (g.id == grocery.id));
                if (index >= 0) {
                    groceries.splice(index, 1);
                }
            }
            else {
                grocery.id = uuid();
            }

            groceries.push(grocery);
            saveGroceries();
        },
        remove(id) {
            let index = groceries.findIndex((grocery) => (grocery.id == id));
            if (index >= 0) {
                groceries.splice(index, 1);
                saveGroceries();
            }
        },
        find(id) {
            return groceries.find((grocery) => (grocery.id == id));
        },
        list() {
            return groceries;
        },
    },

    groceryCategories: {
        save(category) {
            if (category.id) {
                let index = groceryCategories.findIndex((c) => (c.id == category.id));
                if (index >= 0) {
                    groceryCategories.splice(index, 1);
                }
            }
            else {
                category.id = uuid();
            }

            groceryCategories.push(category);
            saveGroceryCategories();
        },
        remove(id) {
            groceries.filter((grocery) => (grocery.categoryId == id))
                .forEach((category) => {
                    category.categoryId = null;
                });
            saveGroceries();

            let index = groceryCategories.findIndex((category) => (category.id == id));
            if (index >= 0) {
                groceryCategories.splice(index, 1);
                saveGroceryCategories();
            }
        },
        find(id) {
            return groceryCategories.find((category) => (category.id == id));
        },
        list() {
            return groceryCategories;
        },
        findBySuggestion(groceryName) {
            return groceryCategories.find((category) => {
                if (category.suggestions) {
                    /* eslint-disable-next-line arrow-body-style */
                    return !!category.suggestions.find((suggestion) => {
                        return suggestion.toLowerCase() == groceryName.toLowerCase();
                    });
                }

                return false;
            });
        },
    },
};
