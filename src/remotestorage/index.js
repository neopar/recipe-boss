import RemoteStorage from 'remotestoragejs';

import Recipe from './recipe';

if (process.env.NODE_ENV != 'development') {
    // Hack the redirect url so we don't try to redirect to a file:// url
    // AND so the redirect url isn't the full domain + path
    RemoteStorage.Authorize.getLocation = function() {
        // Emulate window.location
        function Location(href) {
            this.href = href;
        }

        Location.prototype.toString = function() {
            return this.href;
        };

        return new Location(`https://recipes.bhdouglass.com/${window.location.hash}`);
    };
}

const remoteStorage = new RemoteStorage({
    changeEvents: {local: true, remote: true},
    modules: [Recipe],
    // logging: (process.env.NODE_ENV == 'development'),
});

remoteStorage.access.claim(Recipe.name, 'rw');
remoteStorage.setApiKeys({
    dropbox: process.env.VUE_APP_DROPBOX_KEY,
    googledrive: process.env.VUE_APP_GOOGLE_DRIVE_KEY,
});

export default remoteStorage;
